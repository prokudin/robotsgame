/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import robotsgame.model.navigation.CellPosition;
import robotsgame.model.navigation.Direction;
import robotsgame.model.view.Sprite;

/**
 * Класс песка
 *
 * @author Admin
 */
public class Sand extends Landscape {

    /**
     * Конструктор
     *
     * @param field игровое поле
     */
    public Sand(GameField field) {
        super(field);
        _sprite = Sprite.getSprite("sand.png");
    }

    private final Sprite _sprite;

    /**
     * Увеличить количество ходов для пропуска
     *
     * @param robot робот
     */
    @Override
    protected void increaseSteosToSkipCount(Robot robot) {
        robot.increaseStepsToSkip(1);
    }

    /**
     * Вычичлить путь для робота
     *
     * @param robot робот
     * @param direction направление движения
     * @param way путь
     */
    @Override
    public void calcNextPosition(Robot robot, Direction direction, ArrayList<CellPosition> way) {
        if (way.isEmpty()) {
            // если шаг выполняется из песка
            super.calcNextPosition(robot, direction, way);
        } else {
            // если робот зашел в песок
            increaseSteosToSkipCount(robot);
        }
    }

    @Override
    public void draw(Graphics g, int x, int y) {
        _sprite.draw(g, x, y);
    }

    @Override
    public void draw(Graphics g, Point p) {
        _sprite.draw(g, p);
    }

}
