/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 * Спрайт
 *
 * @author Admin
 */
public class Sprite {

    private Image _image; //изображение

    /**
     * Конструктор
     *
     * @param image имя файла
     */
    public Sprite(Image image) {
        this._image = image;
    }

    /**
     * Ширина картинки
     *
     * @return ширина картинки
     */
    public int getWidth() { //получаем ширину картинки
        return _image.getWidth(null);
    }

    /**
     * Высота картинки
     *
     * @return высота картинки
     */
    public int getHeight() { //получаем высоту картинки
        return _image.getHeight(null);
    }

    /**
     * Наhисовать
     *
     * @param g
     * @param x координата x
     * @param y координата y
     */
    public void draw(Graphics g, int x, int y) { //рисуем картинку
        g.drawImage(_image, x, y, null);
    }

    /**
     * Нарисовать
     *
     * @param g
     * @param p точка
     */
    public void draw(Graphics g, Point p) { //рисуем картинку
        g.drawImage(_image, p.x, p.y, null);
    }

    /**
     * загрузка спрайтов из файла
     */
    public static Sprite getSprite(String path) {
        BufferedImage sourceImage = null;

        try {
            URL url = Sprite.class.getClassLoader().getResource(path);
            sourceImage = ImageIO.read(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Sprite sprite = new Sprite(Toolkit.getDefaultToolkit().createImage(sourceImage.getSource()));

        return sprite;
    }
}
