/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model.view;

import robotsgame.model.navigation.CellPosition;
import robotsgame.model.navigation.Direction;
import robotsgame.model.navigation.MiddlePosition;
import robotsgame.model.GameModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import robotsgame.model.DefaultLandscape;
import robotsgame.model.Landscape;
import robotsgame.model.Sand;
import robotsgame.model.Season;
import robotsgame.model.Swamp;
import robotsgame.model.events.GameOverEvent;
import robotsgame.model.events.GameOverListener;
import robotsgame.model.events.NewSeasonEvent;
import robotsgame.model.events.NewSeasonListener;
import robotsgame.model.events.RobotActionEvent;
import robotsgame.model.events.RobotActionListener;

/**
 *
 * @author Admin
 */
public class GameFieldPanel extends JPanel implements KeyListener, ActionListener {

    // ------------------------------ Модель игры ------------------------------
    private GameModel _model;

    // ------------------------------ Размеры ---------------------------------
    private static final int CELL_SIZE = 50;
    private static final int GAP = 2;
    private static final int FONT_HEIGHT = 15;
    private static final int INFO_SIZE = 200;

    // ------------------------- Цветовое оформление ---------------------------
    private static final Color BACKGROUND_COLOR = new Color(175, 255, 175);
    private static final Color GRID_COLOR = Color.GREEN;
    private static final Color FILL_COLOR = Color.LIGHT_GRAY;

    // -------------------------- Кнопки ---------------------------------------
    private static final JButton BTN_NEWGAME = new JButton("Новая игра");

    private static JComboBox BIG_ROBOT_ROW = new JComboBox();
    private static JComboBox BIG_ROBOT_COLUMN = new JComboBox();
    private static JComboBox LITTLE_ROBOT_ROW = new JComboBox();
    private static JComboBox LITTLE_ROBOT_COLUMN = new JComboBox();

    //--------------------------Спрайты----------------------------------------
    private static Sprite BIG_ROBOT_ANGRY;
    private static Sprite BIG_ROBOT_NORMAL;
    private static Sprite BIG_ROBOT_SKIP_STEP;
    private static Sprite LITTLE_ROBOT;
    private static Sprite LITTLE_ROBOT_SKIP_STEP;
    private static Sprite TARGET_POSITION;

    //********************************Позиции роботов******************************
    private static int LITTLE_ROBOT_ROW_POS = 4;
    private static int LITTLE_ROBOT_COLUMN_POS = 2;
    private static int BIG_ROBOT_ROW_POS = 4;
    private static int BIG_ROBOT_COLUMN_POS = 15;

    public GameFieldPanel(GameModel model) {
        _model = model;

        // Инициализация графики
        int width = 2 * GAP + CELL_SIZE * _model.field().width();
        int height = 2 * GAP + CELL_SIZE * _model.field().height();
        setPreferredSize(new Dimension(width + INFO_SIZE, height - GAP * 4));
        setBackground(Color.RED);

        // добавить обработчики событий на кнопку начала игры
        BTN_NEWGAME.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startNewGame();

            }
        });
        BTN_NEWGAME.setFocusable(false);
        BTN_NEWGAME.setFocusPainted(false);

        // загрузить спрайты
        loadSprites();

        // добавить слушателей
        _model.bigRobot().setRobotActionListener(new GameFieldPanel.RepaintByAction());
        _model.littleRobot().setRobotActionListener(new GameFieldPanel.RepaintByAction());
        _model.season().setNewSeasonActionListener(new GameFieldPanel.RepaintByAction());
        _model.setGameOverListener(new GameFieldPanel.RepaintByAction());

        // инициализировать комбо-боксы
        createComboBoxes();

        // добавить слушаетеля клавиатуры
        addKeyListener(this);
    }

    /**
     * Выполняет действия после нажатя кнопки "Новая игра"
     */
    private void startNewGame() {
        actionPerformed(null);
        _model.setLittleRobot(new CellPosition(LITTLE_ROBOT_ROW_POS, LITTLE_ROBOT_COLUMN_POS));
        _model.setBigRobot(new CellPosition(BIG_ROBOT_ROW_POS, BIG_ROBOT_COLUMN_POS));
        _model.start();
        repaint();
        this.setFocusTraversalKeysEnabled(true);
    }

    /**
     * Загрузка спрайтов из файлов
     */
    private void loadSprites() {
        BIG_ROBOT_ANGRY = Sprite.getSprite("big_robot_angry.png");
        BIG_ROBOT_NORMAL = Sprite.getSprite("big_robot_normal.png");
        BIG_ROBOT_SKIP_STEP = Sprite.getSprite("big_robot_skip_step.png");
        LITTLE_ROBOT = Sprite.getSprite("little_robot_normal.png");
        LITTLE_ROBOT_SKIP_STEP = Sprite.getSprite("little_robot_skip_step.png");
        TARGET_POSITION = Sprite.getSprite("door.png");
    }

    /**
     * Инициализирует комбобоксы значениями
     */
    private void createComboBoxes() {
        BIG_ROBOT_ROW = new JComboBox();
        LITTLE_ROBOT_ROW = new JComboBox();
        BIG_ROBOT_COLUMN = new JComboBox();
        LITTLE_ROBOT_COLUMN = new JComboBox();

        for (int i = 1; i <= _model.field().height(); i++) {
            BIG_ROBOT_ROW.addItem(i);
            LITTLE_ROBOT_ROW.addItem(i);
        }
        for (int i = 1; i <= _model.field().width(); i++) {
            BIG_ROBOT_COLUMN.addItem(i);
            LITTLE_ROBOT_COLUMN.addItem(i);
        }

//        BIG_ROBOT_ROW.setFocusable(false);
//        LITTLE_ROBOT_ROW.setFocusable(false);
//        BIG_ROBOT_COLUMN.setFocusable(false);
//        LITTLE_ROBOT_COLUMN.setFocusable(false);
        BIG_ROBOT_ROW.setSelectedIndex(BIG_ROBOT_ROW_POS - 1);
        BIG_ROBOT_COLUMN.setSelectedIndex(BIG_ROBOT_COLUMN_POS - 1);
        LITTLE_ROBOT_ROW.setSelectedIndex(LITTLE_ROBOT_ROW_POS - 1);
        LITTLE_ROBOT_COLUMN.setSelectedIndex(LITTLE_ROBOT_COLUMN_POS - 1);

        BIG_ROBOT_ROW.addActionListener(this);
        BIG_ROBOT_COLUMN.addActionListener(this);
        LITTLE_ROBOT_ROW.addActionListener(this);
        LITTLE_ROBOT_COLUMN.addActionListener(this);
    }

    /**
     * Нарисовать комбобоксы
     */
    private void drawComboBoxes(Graphics g) {
        setLayout(null);

        g.drawString("Позиция маленького робота", 2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.3));

        LITTLE_ROBOT_ROW.setEditable(true);
        LITTLE_ROBOT_ROW.setSize(50, 30);
        LITTLE_ROBOT_ROW.setLocation(2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.32));
        add(LITTLE_ROBOT_ROW);

        LITTLE_ROBOT_COLUMN.setEditable(true);
        LITTLE_ROBOT_COLUMN.setSize(50, 30);
        LITTLE_ROBOT_COLUMN.setLocation(2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 2, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.32));
        add(LITTLE_ROBOT_COLUMN);

        g.drawString("Позиция большого робота", 2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.41));

        BIG_ROBOT_ROW.setEditable(true);
        BIG_ROBOT_ROW.setSize(50, 30);
        BIG_ROBOT_ROW.setLocation(2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.43));
        add(BIG_ROBOT_ROW);

        BIG_ROBOT_COLUMN.setEditable(true);
        BIG_ROBOT_COLUMN.setSize(50, 30);
        BIG_ROBOT_COLUMN.setLocation(2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 2, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.43));
        add(BIG_ROBOT_COLUMN);
    }

    /**
     * Действия при изменении значения одного из комбо-бокса
     */
    public void actionPerformed(ActionEvent e) {

        LITTLE_ROBOT_ROW_POS = LITTLE_ROBOT_ROW.getSelectedIndex() + 1;
        LITTLE_ROBOT_COLUMN_POS = LITTLE_ROBOT_COLUMN.getSelectedIndex() + 1;
        BIG_ROBOT_ROW_POS = BIG_ROBOT_ROW.getSelectedIndex() + 1;
        BIG_ROBOT_COLUMN_POS = BIG_ROBOT_COLUMN.getSelectedIndex() + 1;

        if (LITTLE_ROBOT_ROW_POS > _model.field().height()) {
            LITTLE_ROBOT_ROW_POS = _model.field().height();
        }
        if (LITTLE_ROBOT_ROW_POS < 0) {
            LITTLE_ROBOT_ROW_POS = 1;
        }
        if (BIG_ROBOT_ROW_POS > _model.field().height()) {
            BIG_ROBOT_ROW_POS = _model.field().height();
        }
        if (BIG_ROBOT_ROW_POS < 0) {
            BIG_ROBOT_ROW_POS = 1;
        }

        if (LITTLE_ROBOT_COLUMN_POS > _model.field().width()) {
            LITTLE_ROBOT_COLUMN_POS = _model.field().width();
        }
        if (LITTLE_ROBOT_COLUMN_POS <= 0) {
            LITTLE_ROBOT_COLUMN_POS = 1;
        }
        if (BIG_ROBOT_COLUMN_POS > _model.field().width()) {
            BIG_ROBOT_COLUMN_POS = _model.field().width();
        }
        if (BIG_ROBOT_COLUMN_POS <= 0) {
            BIG_ROBOT_COLUMN_POS = 1;
        }

        LITTLE_ROBOT_ROW.setSelectedIndex(LITTLE_ROBOT_ROW_POS - 1);
        LITTLE_ROBOT_COLUMN.setSelectedIndex(LITTLE_ROBOT_COLUMN_POS - 1);
        BIG_ROBOT_ROW.setSelectedIndex(BIG_ROBOT_ROW_POS - 1);
        BIG_ROBOT_COLUMN.setSelectedIndex(BIG_ROBOT_COLUMN_POS - 1);

        repaint();
    }

    /**
     * Рисуем поле
     */
    @Override
    public void paintComponent(Graphics g) {

        // Отрисовка фона
        int width = getWidth();
        int height = getHeight();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.BLACK);   // восстнанваливаем цвет пера

        // добавление кнопок
        setLayout(null);
        BTN_NEWGAME.setSize(120, 30);
        BTN_NEWGAME.setLocation(2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.05));
        add(BTN_NEWGAME);

        //отрисовка задания позиции
        //createComboBoxes();
        drawComboBoxes(g);

        // отрисовка ланшафтов
        drawLandscapes(g);

        // отрисовка целевой позиции
        TARGET_POSITION.draw(g, leftTopCell(_model.targetPosition()));

        // отрисовка интформационной панели
        drawSeasonInfo(g);

        // Отрисовка остальных юнитов, стен и дверей
        Point lefTop;
        CellPosition pos = new CellPosition(1, 1);
        Direction direct = Direction.east();
        boolean isPostLastColumn;
        do {
            boolean isPostLastRow;
            do {
                // Отрисовка стен и дверей
                Direction d = Direction.north();
                for (int n = 1; n <= 4; n++) {
                    d = d.clockwise();
                    MiddlePosition mpos = new MiddlePosition(pos, d);
                    if (_model.field().isWall(mpos)) // Отрисовка стены
                    {
                        lefTop = leftTopCell(mpos);
                        drawWall(g, lefTop, mpos.direction());
                    }
                }

                isPostLastRow = !pos.hasNext(direct);
                if (!isPostLastRow) {
                    pos = pos.next(direct);
                }
            } while (!isPostLastRow);

            direct = direct.opposite();

            isPostLastColumn = !pos.hasNext(Direction.south());
            if (!isPostLastColumn) {
                pos = pos.next(Direction.south());
            }
        } while (!isPostLastColumn);

        // отрисовка большого робота
        if (_model.bigRobot().stepsToSkip() > 0) {
            BIG_ROBOT_SKIP_STEP.draw(g, leftTopCell(_model.bigRobot().position()));
        } else if (_model.bigRobot().isActive()) {
            BIG_ROBOT_ANGRY.draw(g, leftTopCell(_model.bigRobot().position()));
        } else {
            BIG_ROBOT_NORMAL.draw(g, leftTopCell(_model.bigRobot().position()));
        }

        // отрисовка маельнького робота
        if (_model.littleRobot().stepsToSkip() == 0) {
            LITTLE_ROBOT.draw(g, leftTopCell(_model.littleRobot().position()));
        } else {
            LITTLE_ROBOT_SKIP_STEP.draw(g, leftTopCell(_model.littleRobot().position()));
        }
    }

    /**
     * Нарисаовать информациюю о текущем времени года
     */
    private void drawSeasonInfo(Graphics g) {
        g.drawString("Время года : " + _model.season().currentSeason().toString(), 2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.15));
        g.drawString("Дней осталось : " + (_model.season().daysInSeason() - _model.season().currentDay()), 2 * GAP + CELL_SIZE * _model.field().width() + INFO_SIZE / 8, (int) ((2 * GAP + CELL_SIZE * _model.field().height()) * 0.20));
    }

    /**
     * Выполняет вывод информации при завершении игры
     */
    private void drawGameOverInfo() {
        if (_model.isPlayerWon()) {
            String str = "Вы победили !!!!";

            JOptionPane.showMessageDialog(null, str, "Победа!", JOptionPane.INFORMATION_MESSAGE);

            //setEnabledField(false);
        } else {
            String str = "Вы проиграли..";

            JOptionPane.showMessageDialog(null, str, "Поражение!", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Рисует ландшафты
     */
    private void drawLandscapes(Graphics g) {

        for (int i = 1; i <= _model.field().height(); i++) {
            for (int j = 1; j <= _model.field().width(); j++) {

                CellPosition p = new CellPosition(i, j);
                Landscape l = _model.field().landscape(p);
                if (l != null) {
                    l.draw(g, leftTopCell(p));
                }
            }
        }
    }

    /**
     * Нарисовать сетку
     */
    private void darwGrid(Graphics g) {
        int width = /*getWidth()*/ (CELL_SIZE + GAP) * _model.field().width();
        int height = /*getHeight()*/ (CELL_SIZE + GAP) * _model.field().height();

        g.setColor(GRID_COLOR/*Color.LIGHT_GRAY*/);

        for (int i = 1; i <= _model.field().width() + 1; i++) // вертикальные линии
        {
            int x = GAP + CELL_SIZE * (i - 1);
            g.drawLine(x, GAP, x, height - 20 + GAP);
        }

        for (int i = 1; i <= _model.field().width() + 1; i++) // горизотнальные линии
        {
            int y = GAP + CELL_SIZE * (i - 1);
            g.drawLine(GAP, y, width - 40 + GAP, y);
        }

    }

    /**
     * Нарисовать стену
     */
    private void drawWall(Graphics g, Point lefTop, Direction direct) {
        g.setColor(Color.black);

        if (direct.equals(Direction.west()) || direct.equals(Direction.east())) {
            g.drawLine(lefTop.x, lefTop.y, lefTop.x, lefTop.y + CELL_SIZE);
            g.drawLine(lefTop.x - GAP / 2, lefTop.y, lefTop.x - GAP / 2, lefTop.y + CELL_SIZE);
            g.drawLine(lefTop.x + GAP / 2, lefTop.y, lefTop.x + GAP / 2, lefTop.y + CELL_SIZE);
        } else {
            g.drawLine(lefTop.x, lefTop.y - GAP / 2, lefTop.x + CELL_SIZE, lefTop.y - GAP / 2);
            g.drawLine(lefTop.x, lefTop.y, lefTop.x + CELL_SIZE, lefTop.y);
            g.drawLine(lefTop.x, lefTop.y + GAP / 2, lefTop.x + CELL_SIZE, lefTop.y + GAP / 2);
        }

        g.setColor(Color.BLACK);   // восстанваливаем цвет пера
    }

    private Point leftTopCell(CellPosition pos) {

        int left = GAP + CELL_SIZE * (pos.column() - 1);
        int top = GAP + CELL_SIZE * (pos.row() - 1);

        return new Point(left, top);
    }

    private Point leftTopCell(MiddlePosition mpos) {

        Point p = leftTopCell(mpos.cellPosition());

        if (mpos.direction().equals(Direction.south())) {
            p.y += CELL_SIZE;
            //p.x += CELL_SIZE;
        } else if (mpos.direction().equals(Direction.east())) {
            p.x += CELL_SIZE;
        }

        return p;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        switch (ke.getKeyCode()) {
            case KeyEvent.VK_UP:                                          // если шаг вверх
                _model.takeStep(Direction.north());
                break;
            case KeyEvent.VK_DOWN:                                        // если шаг вниз
                _model.takeStep(Direction.south());
                break;
            case KeyEvent.VK_LEFT:
                _model.takeStep(Direction.west());                        // если шаг на запад
                break;
            case KeyEvent.VK_RIGHT:
                _model.takeStep(Direction.east());                          // если шаг на восток
                break;
            case KeyEvent.VK_SPACE:                                       // если пропуск хода
                _model.littleRobot().increaseStepsToSkip(1);
                _model.takeStep(Direction.east());
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private class RepaintByAction implements RobotActionListener, NewSeasonListener, GameOverListener {

        @Override
        public void robotMadeAction(RobotActionEvent e) {
            repaint();
        }

        @Override
        public void seasonChanged(NewSeasonEvent e) {
            repaint();
        }

        @Override
        public void gameOver(GameOverEvent e) {
            repaint();
            drawGameOverInfo();
        }
    }
}
