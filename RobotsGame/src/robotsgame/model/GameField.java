/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import robotsgame.model.navigation.MiddlePosition;
import robotsgame.model.navigation.CellPosition;
import java.util.ArrayList;

/**
 * Игровое поле
 *
 * @author Admin
 */
public class GameField {

    private int _width;                            ///ширина 
    private int _height;                           ///высота

    /**
     * Конструктор
     */
    GameField() {
        setSize(15, 10);
        _walls = new ArrayList<WallPiece>();
    }

    /**
     * Задать размер игрового поля
     *
     * @param width ширина
     * @param height высота
     */
    public final void setSize(int width, int height) {
        _width = width;
        _height = height;

        CellPosition.setHorizontalRange(1, width);
        CellPosition.setVerticalRange(1, height);
    }

    /**
     * Ширина
     *
     * @return ширина
     */
    public int width() {
        return _width;
    }

    /**
     * Высота
     *
     * @return высота
     */
    public int height() {
        return _height;
    }

    private LittleRobot _littleRobot;                                   /// маленький робот

    /**
     * Маленький робот
     *
     * @return маленький робот
     */
    public LittleRobot littleRobot() {
        return _littleRobot;
    }

    /**
     * Задать маленького робота
     *
     * @param pos позиция робота
     * @param robot робот
     * @return успешность
     */
    public boolean setLittleRobot(CellPosition pos, LittleRobot robot) {
        if (robot.setPosition(pos)) {
            _littleRobot = robot;
            return true;
        }
        return false;
    }

    private BigRobot _bigRobot;                         /// большой робот

    /**
     * Большой робот
     *
     * @return большой робот
     */
    public BigRobot bigRobot() {
        return _bigRobot;
    }

    /**
     * Задать большого
     *
     * @param pos позиция робота
     * @param robot робот
     * @return успешность
     */
    public boolean setBigRobot(CellPosition pos, BigRobot robot) {
        if (robot.setPosition(pos)) {
            _bigRobot = robot;
            return true;
        }
        return false;
    }

    // ---------------------------- Стены ----------------------------
    private final ArrayList<WallPiece> _walls;

    /**
     * Есть ли стена в заданной позиции
     *
     * @param pos позиция
     * @return есть ли стена
     */
    public boolean isWall(MiddlePosition pos) {

        for (int i = 0; i < _walls.size(); i++) {
            if (_walls.get(i).position().equals(pos)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Добавить стену
     *
     * @param pos позиция стену
     * @param obj стена
     * @return успешность
     */
    public boolean addWall(MiddlePosition pos, WallPiece obj) {

        if (obj.setPosition(pos)) {
            _walls.add(obj);
            return true;
        }
        return false;
    }

    //--------------------------------Ландшафты-------------------------------------
    private ArrayList<Landscape> _landscapes = new ArrayList<Landscape>();

    /**
     * Добавить ландшафт
     *
     * @param landscape ландшафт
     */
    public void addLandscape(Landscape landscape) {
        if (landscape != null) {
            _landscapes.add(landscape);
        }
    }

    /**
     * Ландшафт в заданной позиции
     *
     * @param position позиция
     * @return ландшафт, null если не задан
     */
    public Landscape landscape(CellPosition position) {
        for (Landscape l : _landscapes) {
            if (l.contains(position)) {
                return l;
            }
        }
        return null;
    }

    /**
     * Заполнить клетки без ландшафта стандартным ландшафтом
     */
    public void fillEmptyCells() {

        // удалить старый стандартный ландшафт
        for (int i = 0; i < _landscapes.size(); i++) {
            if (_landscapes.get(i) instanceof DefaultLandscape) {
                _landscapes.remove(i);
            }
        }

        // Создать область ландшафта
        Landscape def = new DefaultLandscape(this);
        ArrayList<CellPosition> area = new ArrayList<CellPosition>();
        for (int i = 1; i <= _height; i++) {
            for (int j = 1; j <= _width; j++) {
                CellPosition p = new CellPosition(i, j);
                if (landscape(p) == null) {
                    area.add(p);
                }
            }
        }

        if (!area.isEmpty()) {
            def.setArea(area);
            addLandscape(def);
        }
    }

    // Время года
    private Season _season;

    /**
     * Время года
     *
     * @return время года
     */
    public Season season() {
        return _season;
    }

    /**
     * Установить время года
     *
     * @param season новое время года
     */
    public void setSeason(Season season) {
        _season = season;
    }

    /**
     * Выполнить действия при смене сезона
     */
    public void nextSeasonAction() {
        for (int i = 0; i < _landscapes.size(); i++/*Landscape l : _landscapes*/) {
            _landscapes.get(i).nextSeasonAction();
        }
    }

    /**
     * Очистить игровое поле
     */
    public void clear() {
        _landscapes.clear();
        _walls.clear();
        _bigRobot = null;
        _littleRobot = null;
        _season = null;
    }
}
