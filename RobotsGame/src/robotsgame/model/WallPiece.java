package robotsgame.model;

import robotsgame.model.navigation.Direction;
import robotsgame.model.navigation.MiddlePosition;

/*
 * WallPiece - участок стены длиной не более одной ¤чейки
 */
public class WallPiece {

    private final GameField _field;

    /**
     * Конструктор
     *
     * @param field игровое поле
     */
    public WallPiece(GameField field) {
        _field = field;
    }

    // ----------------------- ѕозици¤ стены -------------------------
    private MiddlePosition _position;

    /**
     * Позиция стены
     *
     * @return позиция стены
     */
    public MiddlePosition position() {
        return _position;
    }

    /**
     * Установить позицию стены
     *
     * @param pos
     * @return
     */
    boolean setPosition(MiddlePosition pos) {
        if (!_field.isWall(pos)) {
            _position = pos;
            return true;
        }

        return false;
    }

    // ----------------------- ќриентаци¤ стены -------------------------
    public static final int VERTICAL = 1;          ///Вертикальная ориентация стены
    public static final int HORIZONTAL = 2;        ///Горизонтальная ориентация тсены

    /**
     * Ориентация стены
     *
     * @return ориентация стены
     */
    public int orientation() {
        Direction direct = position().direction();

        if (direct.equals(Direction.south()) || direct.equals(Direction.north())) {
            return VERTICAL;
        }
        if (direct.equals(Direction.west()) || direct.equals(Direction.east())) {
            return HORIZONTAL;
        }

        // TODO »сключение
        throw new RuntimeException("Invalid orientation");
    }
}
