/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import robotsgame.model.navigation.CellPosition;
import robotsgame.model.navigation.Direction;
import robotsgame.model.view.Sprite;

/**
 * Класс болота
 *
 * @author Admin
 */
public class Swamp extends Landscape {

    /**
     * Конструктор
     *
     * @param field
     */
    public Swamp(GameField field) {
        super(field);

        _frozen = Sprite.getSprite("frozen_swamp.png");
        _normal = Sprite.getSprite("sprite_swamp.png");
        _isFrosen = (field.season().currentSeason().equals(Season.Seasons.WINTER));
    }

    Sprite _frozen;
    Sprite _normal;

    private boolean _isFrosen;

    /**
     * Зморожено ли болото
     *
     * @return заморожено ли болото
     */
    public boolean isFrosen() {
        return _isFrosen;
    }

    /**
     * Увеличить число ходо для пропуска
     *
     * @param robot робот
     */
    @Override
    protected void increaseSteosToSkipCount(Robot robot) {
        if (robot instanceof BigRobot) {
            robot.increaseStepsToSkip(3);
        }
    }

    /**
     * Может ли робот двигаться в этот ландшафт
     *
     * @param robot робот
     * @return может ли робот двигаться в этот ландшафт
     */
    @Override
    public boolean canMoveInto(Robot robot) {
        return (robot instanceof BigRobot) || (robot instanceof LittleRobot && isFrosen());
    }

    /**
     * Действие при смене сезодна
     */
    @Override
    protected void nextSeasonAction() {
        if (field().season().currentSeason().equals(Season.Seasons.AUTUMN)) {
            increaseArea();
        }

        if (field().season().currentSeason().equals(Season.Seasons.WINTER)) {
            decreaseArea();
            _isFrosen = true;
        }

        if (field().season().currentSeason().equals(Season.Seasons.SPRING)) {
            increaseArea();
            _isFrosen = false;
        }

        if (field().season().currentSeason().equals(Season.Seasons.SUMMER)) {
            decreaseArea();
        }
    }

    /**
     * Вычислить путь для робота
     *
     * @param robot робот
     * @param direction направление движения
     * @param way путь
     */
    @Override
    public void calcNextPosition(Robot robot, Direction direction, ArrayList<CellPosition> way) {
        if (!canMoveInto(robot)) {
            return;
        }

        if (way.isEmpty()) {
            // если робот находится в болоте
            super.calcNextPosition(robot, direction, way);
        } else if (isFrosen()) {
            // если болото заморожено
            // добавить новую позицию к маршруту
            CellPosition pos = way.get(way.size() - 1).hasNext(direction) ? way.get(way.size() - 1).next(direction) : null;
            if (pos == null) {
                return;
            }
            way.add(pos);
        } else {
            // увеличить колчиство пропускаемых ходов
            increaseSteosToSkipCount(robot);
        }
    }

    /**
     * Может ли клетка увеличиться в размерах
     *
     * @param pos клетка
     * @return Может ли клетка увеличиться в размерах
     */
    private boolean canGrowUp(CellPosition pos) {
        boolean canGrow = true;
        for (int i = pos.row() - 1; i <= pos.row() + 1; i++) {
            for (int j = pos.column() - 1; j <= pos.column() + 1; j++) {
                canGrow &= CellPosition.isValid(i, j) && (field().landscape(new CellPosition(i, j)) instanceof DefaultLandscape || field().landscape(new CellPosition(i, j)) instanceof Swamp);
            }
        }
        return canGrow;
    }

    /**
     * Может ли область вокруг клетки уменьшиться в размерах
     *
     * @param pos клетка
     * @return может ли область вокруг клетки уменьшиться в размерах
     */
    private boolean canDecrease(CellPosition pos) {
        boolean canDecrease = true;
        for (int i = pos.row() - 1; i <= pos.row() + 1 && canDecrease; i++) {
            for (int j = pos.column() - 1; j <= pos.column() + 1 && canDecrease; j++) {
                canDecrease &= CellPosition.isValid(i, j) && (_area.contains(new CellPosition(i, j)));
            }
        }
        return canDecrease;
    }

    /**
     * Найти область после увеличения размеров
     *
     * @return новая область
     */
    private ArrayList<CellPosition> findAreaAfterIncrease() {
        ArrayList<CellPosition> newArea = new ArrayList<CellPosition>();
        newArea.addAll(_area);

        for (CellPosition pos : _area) {                 // для каждой позции

            if (canGrowUp(pos)) {
                for (int i = pos.row() - 1; i <= pos.row() + 1; i++) {
                    for (int j = pos.column() - 1; j <= pos.column() + 1; j++) {
                        if (!newArea.contains(new CellPosition(i, j))) {
                            newArea.add(new CellPosition(i, j));
                        }
                    }
                }
            }
        }

        return newArea;
    }

    private ArrayList<CellPosition> _normalArea;
    private ArrayList<CellPosition> _increasedArea;

    /**
     * Найти область после уменьшения размеров
     *
     * @return область после уменьшения размеров
     */
    private ArrayList<CellPosition> findAreaAfterDecrease() {
        ArrayList<CellPosition> newArea = new ArrayList<CellPosition>();

        for (CellPosition pos : _area) {                // для каждой позции                        
            if (canDecrease(pos)) {
                newArea.add(pos);
            }
        }

        return newArea;
    }

    /**
     * Увеличить размер ландшафта
     */
    protected void increaseArea() {
        _normalArea = _area;

        if (_increasedArea == null) {
            _increasedArea = findAreaAfterIncrease();
        }
        _area = _increasedArea;
    }

    /**
     * Увеличить размер ландщафта
     */
    protected void decreaseArea() {
        _increasedArea = _area;

        if (_normalArea == null) {
            _normalArea = findAreaAfterDecrease();
        }

        _area = _normalArea;
    }

    @Override
    public void draw(Graphics g, int x, int y) {
        if (isFrosen()) {
            _frozen.draw(g, x, y);
        } else {
            _normal.draw(g, x, y);
        }
    }

    @Override
    public void draw(Graphics g, Point p) {
        if (isFrosen()) {
            _frozen.draw(g, p);
        } else {
            _normal.draw(g, p);
        }
    }

}
