/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.util.ArrayList;
import robotsgame.model.navigation.CellPosition;
import robotsgame.model.navigation.Direction;

/**
 * Класс большого робота
 *
 * @author Admin
 */
public class BigRobot extends Robot {

    public BigRobot(GameField field) {
        super(field);
        _isActive = false;
    }

    private boolean _isActive;  /// Находится ли робот в активном состоянии

    /**
     * Находится ли робот в активном состоянии
     *
     * @return робот в активном состоянии
     */
    public boolean isActive() {
        return _isActive;
    }

    private ArrayList<CellPosition> _way = new ArrayList<CellPosition>();                 ///путь

    /**
     * Входит ли маленький робот в зону видимости
     *
     * @return
     */
    public boolean isLittleRobotVisible() {
        CellPosition lrPos = field().littleRobot().position();

        return (Math.abs(position().row() - lrPos.row()) <= 1 && Math.abs(position().column() - lrPos.column()) <= 1);
    }

    /**
     * Сделать шаг большим роботом в активном состоянии
     */
    private void makeMovieAtActiveState() {

        // найти направление очередного шага
        CellPosition pos = _way.get(0);
        Direction d = position().direction(pos);
        int r = 2;
        if (d != null) {
            r = super.makeMovie(d);
        }

        if (r != 2) { // если шаг был сделан
            int index = _way.indexOf(position());

            if (index != -1) {
                _way.subList(0, index + 1).clear();
            }
        }

        _isActive = !(_way.isEmpty() || r == 2);
    }

    /**
     * Сделать шаг роботом в пассивном состоянии
     */
    private void makeMovieAtPassiveState() {

        if (isLittleRobotVisible()) {
            // если робот в зоне видимости
            // найти путь до робота
            _way.clear();
            findWay(position(), field().littleRobot().position());

            // сделать шаг
            makeMovie();

            return;
        }

        CellPosition lrPos = field().littleRobot().position();  // позиция маленького робота
        Direction d;
        int r = 2;

        if (position().row() > lrPos.row()) { // если робот сверху
            d = Direction.north();
            r = super.makeMovie(d);
        }

        if (r == 2 && position().row() < lrPos.row()) { // если шаг не был сделан и робот снизу
            d = Direction.south();
            r = super.makeMovie(d);
        }

        if (r == 2 && position().column() > lrPos.column()) { // если шаг не был сделан и робот слева
            d = Direction.west();
            r = super.makeMovie(d);
        }

        if (r == 2 && position().column() < lrPos.column()) { // если шаг не был сделан и робот справа
            d = Direction.east();
            r = super.makeMovie(d);
        }

        if (isLittleRobotVisible()) {
            // если робот в зоне видимости
            // найти путь до робота
            _way.clear();
            findWay(position(), field().littleRobot().position());
        }
    }

    /**
     * Сделать шаг роботом
     */
    public void makeMovie() {
        if (isActive()) {
            makeMovieAtActiveState();
        } else {
            makeMovieAtPassiveState();
        }
    }

    private ArrayList<CellPosition> stack = new ArrayList<CellPosition>();                 /// множество пройденных ячеек

    /**
     * Поиск пути до маленького робота
     *
     * @param begin исходная позиция
     * @param end целевая позиция
     */
    private void findWay(CellPosition begin, CellPosition end) {

        LinkedList<CellPosition> frontier  = new LinkedList<>();
        
        frontier.add(begin);
        
        HashMap<CellPosition,CellPosition> cameFrom = new HashMap<>();
        
        cameFrom.put(begin, null);
        
        CellPosition current = null,next = null;
        Direction direction = null;  
        
        while(!frontier.isEmpty()){    
            
            current = frontier.removeFirst();

            if (current.equals(end))
                break;
            
            direction = Direction.north();
            
            for(int n = 1; n<=4 ; n++){
                direction = direction.clockwise();
                
                if (current.hasNext(direction)){
                    next = current.next(direction);
                    
                    if (!cameFrom.containsKey(next) && super.canMoveToPosition(current,next))
                    {
                        frontier.add(next);
                        cameFrom.put(next, current);
                    }
                }                
            }
        }
        
        _way.clear();
        
        CellPosition prev=null;
        current = end;
        _way.add(current);
        
        do
        {
            prev = cameFrom.get(current);
            
            if (prev != null && !prev.equals(begin))
                _way.add(0,prev);
            
            current = prev;
        }while (prev!=null);
        
        
        _isActive = _way.size() > 0;
    }
}
