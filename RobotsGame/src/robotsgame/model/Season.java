/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import robotsgame.model.events.NewSeasonEvent;
import robotsgame.model.events.NewSeasonListener;

/**
 * Класс времени года
 *
 * @author Admin
 */
public class Season {

    private final GameField _field;

    /**
     * Конструктор
     *
     * @param field игровое поле
     */
    public Season(GameField field) {
        _field = field;
        _daysInSeason = 30;
        _currentDay = 0;
        _currentSeason = Seasons.SUMMER;
    }

    // количество дней в одном времени года
    private static int _daysInSeason = 30;

    /**
     * Количество дней во времени года
     *
     * @return
     */
    public int daysInSeason() {
        return _daysInSeason;
    }

    /**
     * Установить количество дней в сезоне
     *
     * @param daysCount количество дней
     */
    public void setDaysInSeason(int daysCount) {
        _daysInSeason = daysCount > 0 ? daysCount : 10;
    }

    /**
     * Времена года
     */
    static public enum Seasons {
        WINTER, SPRING, SUMMER, AUTUMN
    };
    private Seasons _currentSeason;

    /**
     * Текущее время года
     *
     * @return
     */
    public Seasons currentSeason() {
        return _currentSeason;
    }

    /**
     * Установить текущеее время года
     *
     * @param season
     */
    public void setCurrentSeason(Seasons season) {
        if (_currentSeason != null) {
            if (!_currentSeason.equals(season)) {
                _currentSeason = season;
                _field.nextSeasonAction();
            }
        } else {
            _currentSeason = season;
        }
    }

    /**
     * Следующее время года
     */
    public void nextSeason() {
        switch (_currentSeason) {
            case WINTER:
                _currentSeason = Seasons.SPRING;
                break;
            case SPRING:
                _currentSeason = Seasons.SUMMER;
                break;
            case SUMMER:
                _currentSeason = Seasons.AUTUMN;
                break;
            case AUTUMN:
                _currentSeason = Seasons.WINTER;
                break;
        }

        _field.nextSeasonAction();
    }

    // текущий день
    private int _currentDay = 0;

    /**
     * Текущий день
     *
     * @return теукщий день
     */
    public int currentDay() {
        return _currentDay;
    }

    /**
     * Установить текущий день
     *
     * @param day текущий день
     */
    public void setCurrentDay(int day) {
        _currentDay = day > 0 && day <= _daysInSeason ? day : 0;
    }

    /**
     * Перейти к следующему дню
     */
    public void nextDay() {
        _currentDay++;

        if (_currentDay > _daysInSeason) {
            nextSeason();
            _seasonListener.seasonChanged(new NewSeasonEvent(this));
            _currentDay = 1;
        }
    }

    //дествия, связанные с времнами года
    private NewSeasonListener _seasonListener;

    /**
     * Установить слушателя о смене времени года
     *
     * @param a
     */
    public void setNewSeasonActionListener(NewSeasonListener a) {
        _seasonListener = a;
    }
}
