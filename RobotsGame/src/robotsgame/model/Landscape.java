/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import robotsgame.model.navigation.Direction;
import robotsgame.model.navigation.CellPosition;

/**
 * Класс Ландшафта
 *
 * @author Admin
 */
public abstract class Landscape {

    private final GameField _field;    /// игровое поле

    /**
     * Игровое поле
     *
     * @return игровое поле
     */
    protected GameField field() {
        return _field;
    }

    /**
     * Конструктор
     *
     * @param field
     */
    public Landscape(GameField field) {
        _field = field;
    }

    // область
    protected ArrayList<CellPosition> _area = new ArrayList<CellPosition>();

    /**
     * Установить область
     *
     * @param area область
     */
    public void setArea(ArrayList<CellPosition> area) {
        if (!area.isEmpty()) {
            _area = area;
        }
        // TODO исключение
    }

    /**
     * Принадлежит ли позиция ландшафту
     *
     * @param position позиция
     * @return принадлежность
     */
    public boolean contains(CellPosition position) {
        return _area.contains(position);
    }

    /**
     * Сменить напраление движения робота
     *
     * @param robot робот
     * @param direction направление
     */
    protected void changeDirection(Robot robot, Direction direction) {

    }

    /**
     * Действия при смене времения года
     */
    protected void nextSeasonAction() {

    }

    /**
     * Увелитчить число ходов дляч пропуска
     *
     * @param robot робот
     */
    protected void increaseSteosToSkipCount(Robot robot) {

    }

    /**
     * Может ли робот двигаться в этом ландшафте
     *
     * @param robot робот
     * @return может ли
     */
    public boolean canMoveInto(Robot robot) {
        return true;
    }

    /**
     * Рассчитать путь робота при движении через ландшафт
     *
     * @param robot робот
     * @param direction направление движения
     * @param way путь
     */
    protected void calcNextPosition(Robot robot, Direction direction, ArrayList<CellPosition> way) {
        if (way.isEmpty()) {
            // если робот делает первы шаг из клетки с заданным ландшафтром        
            // сменить направление
            changeDirection(robot, direction);

            if (canMoveInto(robot)) {
                // если робот может двигаться в заданном ландшаяте

                // найти новую позицию
                CellPosition pos = robot.position().hasNext(direction) ? robot.position().next(direction) : null;
                if (pos == null) {
                    return;
                }

                //добавить позицию в путь
                way.add(pos);

                // найти ландшафт новой позиции
                Landscape newLandscape = field().landscape(pos);

                // выполнить поиск пути робота для следующего ланшафта
                newLandscape.calcNextPosition(robot, direction, way);
            }
        }
    }

    /**
     * Рисует ландшафт в заданную позицию
     *
     * @param g
     * @param x координата x
     * @param y координата y
     */
    public abstract void draw(Graphics g, int x, int y);

    /**
     * Рисует ландшафт в заданную позицию
     *
     * @param g
     * @param p позиция
     */
    public abstract void draw(Graphics g, Point p);

    /**
     * Найти путь робота при движении в заданном направлении по текущему
     * маршруту
     *
     * @param robot робот
     * @param direction направление
     * @return путь
     */
    public ArrayList<CellPosition> findWay(Robot robot, Direction direction) {
        ArrayList<CellPosition> way = new ArrayList<CellPosition>();

        calcNextPosition(robot, direction, way);

        return way;
    }
}
