package robotsgame.model.events;

import java.util.EventObject;

/**
 * Событие, св¤занное с движение робота
 *
 * @author Admin
 */
public class RobotActionEvent extends EventObject {

    public RobotActionEvent(Object source) {
        super(source);
    }
}
