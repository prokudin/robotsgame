package robotsgame.model.events;

import java.util.EventListener;

/**
 * Слушатель движения робота
 *
 * @author Admin
 */
public interface RobotActionListener extends EventListener {

    void robotMadeAction(RobotActionEvent e);
}
