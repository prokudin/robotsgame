/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model.events;

import java.util.EventObject;

/**
 * Событие о завршении игры
 *
 * @author Admin
 */
public class GameOverEvent extends EventObject {

    public GameOverEvent(Object source) {
        super(source);
    }
}
