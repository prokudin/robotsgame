/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model.events;

import java.util.EventListener;

/**
 * Слушатель о завршении игры
 *
 * @author Admin
 */
public interface GameOverListener extends EventListener {

    void gameOver(GameOverEvent e);
}
