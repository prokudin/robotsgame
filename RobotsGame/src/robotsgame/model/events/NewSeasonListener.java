package robotsgame.model.events;

import java.util.EventListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Слушатель о смене времени года
 *
 * @author Admin
 */
public interface NewSeasonListener extends EventListener {

    void seasonChanged(NewSeasonEvent e);
}
