/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import robotsgame.model.view.Sprite;

/**
 * Стандартный ландшафт
 *
 * @author Admin
 */
public class DefaultLandscape extends Landscape {

    public DefaultLandscape(GameField field) {
        super(field);
        _sprites = new HashMap<Season.Seasons, Sprite>();

        _sprites.put(Season.Seasons.WINTER, Sprite.getSprite("default_winter.png"));
        _sprites.put(Season.Seasons.SPRING, Sprite.getSprite("default_spring.png"));
        _sprites.put(Season.Seasons.SUMMER, Sprite.getSprite("default.png"));
        _sprites.put(Season.Seasons.AUTUMN, Sprite.getSprite("default_autumn.png"));
    }

    private final HashMap<Season.Seasons, Sprite> _sprites;

    /**
     * Рисует ландшафт в заданную позицию
     *
     * @param g
     * @param x координата x
     * @param y координата y
     */
    @Override
    public void draw(Graphics g, int x, int y) {
        _sprites.get(field().season().currentSeason()).draw(g, x, y);
    }

    /**
     * Рисует ландшафт в заданную позицию
     *
     * @param g
     * @param p позиция
     */
    @Override
    public void draw(Graphics g, Point p) {
        _sprites.get(field().season().currentSeason()).draw(g, p);
    }

}
