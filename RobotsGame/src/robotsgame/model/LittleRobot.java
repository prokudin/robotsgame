/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import robotsgame.model.navigation.Direction;

/**
 * Класс маленького робота
 *
 * @author Admin
 */
public class LittleRobot extends Robot {

    /**
     * Конструктор
     *
     * @param field
     */
    public LittleRobot(GameField field) {
        super(field);
    }

    /**
     * Сделать шаг в заданном направлении
     *
     * @param direct направление
     * @return 0 если успешно, 1 если пропуск хода, 2 если без движения
     */
    @Override
    public int makeMovie(Direction direct) {
        return super.makeMovie(direct);
    }
}
