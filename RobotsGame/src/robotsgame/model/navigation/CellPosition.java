package robotsgame.model.navigation;

import java.util.HashMap;

/**
 * Позиция ячейки
 *
 * @author Admin
 */
public class CellPosition {
    // -- Диапазоны возможных значений по горизонтали и вертикали для всех позиций --

    private static CellRange _horizontalRange = new CellRange(0, 0);
    private static CellRange _verticalRange = new CellRange(0, 0);

    /**
     * Установить горизонтальные границы
     *
     * @param min минимум
     * @param max максимум
     */
    public static void setHorizontalRange(int min, int max) {
        if (CellRange.isValidRange(min, max)) {
            _horizontalRange = new CellRange(min, max);
        }
    }

    /**
     * Горизонтальные границы
     *
     * @return горизонтальные границы
     */
    public static CellRange horizontalRange() {
        return _horizontalRange;
    }

    /**
     * Установить вертикальные границы
     *
     * @param min минимум
     * @param max максимуму
     */
    public static void setVerticalRange(int min, int max) {
        if (CellRange.isValidRange(min, max)) {
            _verticalRange = new CellRange(min, max);
        }
    }

    /**
     * Вертикалные границы
     *
     * @return вертикальная граница
     */
    public static CellRange verticalRange() {
        return _verticalRange;
    }

    // ------------------ Позиция внутри диапазона ---------------------
    private int _row;// = _verticalRange.min();
    private int _column;// = _horizontalRange.min();

    /**
     * Конструктор
     *
     * @param row строка
     * @param col колонка
     */
    public CellPosition(int row, int col) {
        if (!isValid(row, col)) {  //  TODO породить исключение 
            throw new IllegalArgumentException("Invalid position");
        }

        _row = row;
        _column = col;
    }

    /**
     * Строка
     *
     * @return строка
     */
    public int row() {

        if (!isValid()) {  //  TODO породить исключение 
            throw new IllegalArgumentException("Invalid position");
        }

        return _row;
    }

    /**
     * Колонка
     *
     * @return колонка
     */
    public int column() {

        if (!isValid()) {  //  TODO породить исключение 
            throw new IllegalArgumentException("Invalid position");
        }

        return _column;
    }

    /**
     * Валидность позиции
     *
     * @return валидность
     */
    public boolean isValid() {
        return isValid(_row, _column);
    }

    /**
     * Валидность
     *
     * @param row строка
     * @param col колонка
     * @return валидность
     */
    public static boolean isValid(int row, int col) {
        return _horizontalRange.contains(col) && _verticalRange.contains(row);
    }

    /**
     * Клонировать
     *
     * @return клонированный класс
     */
    @Override
    public CellPosition clone() {
        return new CellPosition(_row, _column);
    }

    /**
     * Следующая ячейка
     *
     * @param direct направлени
     * @return следующая ячейка
     */
    public CellPosition next(Direction direct) {

        int[] newPos = calcNewPosition(_row, _column, direct);
        return new CellPosition(newPos[0], newPos[1]);
    }

    /**
     * Есть ли клетка в заданном направлении
     *
     * @param direct навправление
     * @return есть ли клетка в заданном направлении
     */
    public boolean hasNext(Direction direct) {

        int[] newPos = calcNewPosition(_row, _column, direct);
        return isValid(newPos[0], newPos[1]);
    }

    // Вовзвращает массив из двух элементов: индекс строки, индекс столбца
    private int[] calcNewPosition(int row, int col, Direction direct) {

        // Таблица смещения для различных направлений: (горизонталь,вертикаль)
        HashMap<Direction, int[]> offset = new HashMap<Direction, int[]>();

        offset.put(Direction.north(), new int[]{0, -1});
        offset.put(Direction.south(), new int[]{0, 1});
        offset.put(Direction.east(), new int[]{1, 0});
        offset.put(Direction.west(), new int[]{-1, 0});

        int[] newPos = new int[2];

        newPos[0] = _row + offset.get(direct)[1];
        newPos[1] = _column + offset.get(direct)[0];

        return newPos;
    }

    // ------------------ Сравнение позиций ---------------------
    @Override
    public boolean equals(Object other) {

        if (!isValid()) {  //  TODO породить исключение 
            throw new IllegalArgumentException("Invalid position");
        }

        if (other instanceof CellPosition) {
            // Типы совместимы, можно провести преобразование
            CellPosition otherPosition = (CellPosition) other;
            // Возвращаем результат сравнения углов
            return _row == otherPosition._row && _column == otherPosition._column;
        }

        return false;
    }

    /**
     * Направление между соседними позициями
     *
     * @param other другая позиция
     * @return направление, null если нет
     */
    public Direction direction(CellPosition other) {

        if (row() == other.row() && column() > other.column()) {
            return Direction.west();
        }
        if (row() == other.row() && column() < other.column()) {
            return Direction.east();
        }
        if (row() > other.row() && column() == other.column()) {
            return Direction.north();
        }
        if (row() < other.row() && column() == other.column()) {
            return Direction.south();
        }

        return null;
    }

}
