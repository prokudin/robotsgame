package robotsgame.model.navigation;

/**
 * Допустимый дипазон ячеек
 *
 * @author Admin
 */
public class CellRange {

    // ------------------ Возможные значения ------------------
    private int _min = 0;
    private int _max = 0;

    /**
     * Конструктор
     *
     * @param min минимум
     * @param max максимум
     */
    public CellRange(int min, int max) {
        if (min < 0) {
            min = 0;
        }
        if (max < min) {
            max = min;
        }

        _min = min;
        _max = max;
    }

    /**
     * минимум
     *
     * @return минимум
     */
    public int min() {
        return _min;
    }

    /**
     * максимум
     *
     * @return максимум
     */
    public int max() {
        return _max;
    }

    /**
     * длина
     *
     * @return длина
     */
    public int length() {
        return _max - _min + 1;
    }

    /**
     * Валидный ли диапазон
     *
     * @param min минимум
     * @param max максимум
     * @return валидный ли диапазон
     */
    public static boolean isValidRange(int min, int max) {
        return min > 0 && max >= min;
    }

    // ------------------ Принадлежность диапазону ------------------
    /**
     * Содержится ли в диацазоне
     *
     * @param val номер
     * @return содержится ли в диацазоне
     */
    public boolean contains(int val) {
        return val >= _min && val <= _max;
    }
}
