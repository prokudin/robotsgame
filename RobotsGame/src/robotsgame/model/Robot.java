/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.util.ArrayList;
import robotsgame.model.events.RobotActionEvent;
import robotsgame.model.navigation.Direction;
import robotsgame.model.navigation.CellPosition;
import robotsgame.model.events.RobotActionListener;
import robotsgame.model.navigation.MiddlePosition;

/**
 * Класс робота
 *
 * @author Admin
 */
public abstract class Robot {

    private final GameField _field;

    /**
     * Игровое поле
     *
     * @return игровое поле
     */
    protected GameField field() {
        return _field;
    }

    /**
     * Конструктор
     *
     * @param field игровое поле
     */
    public Robot(GameField field) {
        _field = field;
        _stepsToSkip = 0;
    }

    // ------------------- Позиция робота -----------------
    private CellPosition _position;

    /**
     * Позиция робота
     *
     * @return позиция робота
     */
    public CellPosition position() {
        return _position;
    }

    /**
     * Установить позицию робота
     *
     * @param pos позиция
     * @return успешность
     */
    boolean setPosition(CellPosition pos) {
        if (pos.isValid()) {
            _position = pos;
            return true;
        }
        return false;
    }

    // Количество пропускаемых ходов
    private int _stepsToSkip;

    /**
     * Количество пропускаемых ходов
     *
     * @return количество пропускаемых ходов
     */
    public int stepsToSkip() {
        return _stepsToSkip;
    }

    /**
     * Увеличить количество пропускаемых ходов
     *
     * @param steps количество ходов для пропуска
     */
    public void increaseStepsToSkip(int steps) {
        if (steps >= 0) {
            _stepsToSkip += steps;
        } else {
            throw new IllegalArgumentException("steps count < 0");
        }
    }

    /**
     * Жив ли робот
     *
     * @return жив ли робот
     */
    public boolean isAlive() {
        return /*_isAlive*/ field().landscape(position()).canMoveInto(this);
    }

    /**
     * Сделать шаг роботом в указанном направлении
     *
     * @param direct
     * @return 0 если робот двигается, 1 если пропускает ход, 2 - если не
     * двигется
     */
    protected int makeMovie(Direction direct) {

        if (stepsToSkip() > 0) {// пропустить ход
            _stepsToSkip--;
            _actionListener.robotMadeAction(new RobotActionEvent(this));
            return 1;
        }

        // найти ланшафт на текущей позиции робота и рассчитать путь
        Landscape lanscape = field().landscape(position());
        ArrayList<CellPosition> way = lanscape.findWay(this, direct);

        boolean canMovie = true, movieDone = false;
        for (int i = 0; i < way.size() && canMovie && isAlive(); i++) {
            // пока движение возможно
            canMovie = canMoveToPosition(position(), way.get(i));

            if (canMovie) {
                movieDone = true;
                setPosition(way.get(i));
                _actionListener.robotMadeAction(new RobotActionEvent(this));
            }
        }

        //если движение произошло или робот мертв
        if (movieDone || !isAlive()) {
            return 0;
        }

        _stepsToSkip = 0;
        return 2;
    }

    /**
     * Может ли робот переместиться в заданную позацию
     *
     * @param from позиция
     * @param to позиия, заходящаяся в соседней клетке
     * @return может ли робот переместиться в новую позицию
     */
    protected boolean canMoveToPosition(CellPosition from, CellPosition to) {

        if (!field().landscape(to).canMoveInto(this)) {
            return false;
        }

        Direction direct = from.direction(to);

        if (direct == null) {
            return false;
        }

        MiddlePosition mpos = new MiddlePosition(from, direct);
        if (field().isWall(mpos)) {
            return false;
        }

        return true;
    }

    // слушатели    
    private RobotActionListener _actionListener;

    /**
     * Установить слушателя для движения робота
     *
     * @param a
     */
    public void setRobotActionListener(RobotActionListener a) {
        _actionListener = a;
    }
}
