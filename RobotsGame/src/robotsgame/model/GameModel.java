/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package robotsgame.model;

import java.util.ArrayList;
import robotsgame.model.events.GameOverEvent;
import robotsgame.model.events.GameOverListener;
import robotsgame.model.navigation.CellPosition;
import robotsgame.model.navigation.Direction;
import robotsgame.model.navigation.MiddlePosition;

/**
 * Еласс модели игры
 *
 * @author Admin
 */
public class GameModel {

    /**
     * Конструктор
     */
    public GameModel() {
        _field = new GameField();
        _littleRobot = new LittleRobot(_field);
        _bigRobot = new BigRobot(_field);
        _gameOver = false;
        _playerWon = false;
        _season = new Season(_field);
    }

    // -----------------------Игровое поле------------------
    private final GameField _field;

    public GameField field() {
        return _field;
    }

    private final LittleRobot _littleRobot;

    /**
     * Маленький робот
     *
     * @return маленький робот
     */
    public LittleRobot littleRobot() {
        return _littleRobot;
    }

    // Большой робот
    private final BigRobot _bigRobot;

    /**
     * Большой робот
     *
     * @return большой робот
     */
    public BigRobot bigRobot() {
        return _bigRobot;
    }

    // сезон года
    private Season _season;

    /**
     * Сезон года
     *
     * @return сезон года
     */
    public Season season() {
        return _season;
    }

    public void start() {
        _gameOver = false;
        _playerWon = false;

        generateField();

        // Вдруг игра завершилась, еще не начавшись
        identifyGameOver();
    }

    // -------------------- Целевая позиция мальнького робота --------------------------
    private CellPosition _targetPos;

    /**
     * Целевая позиция маленького робота
     *
     * @return целевая позиция маленького робота
     */
    public CellPosition targetPosition() {
        return _targetPos;
    }

    /**
     * Сделать шаг игрового цикла
     *
     * @param littleRobotDirection направление движения маленького робота
     */
    public void takeStep(Direction littleRobotDirection) {
        if (_gameOver) {
            return;
        }

        // сделать шаг маленьким роботом
        int tmp = littleRobot().makeMovie(littleRobotDirection);
        if (tmp == 2) //если шаг не сделан
        {
            return;
        }

        // проверить игру на завершение
        identifyGameOver();

        if (_gameOver) {
            return;
        }

        // сделать шаг большим роботом                
        bigRobot().makeMovie();

        // проверить игру на завершение
        identifyGameOver();

        if (_gameOver) {
            return;
        }

        // сделать следующий день
        _season.nextDay();

        // проверить игру на завершение               
        identifyGameOver();
    }

    /**
     * Установить позацию маленького робота
     *
     * @param pos позиция
     */
    public void setLittleRobot(CellPosition pos) {
        _field.setLittleRobot(pos, littleRobot());
    }

    /**
     * Установить позацию большого робота
     *
     * @param pos позиция
     */
    public void setBigRobot(CellPosition pos) {
        _field.setBigRobot(pos, bigRobot());
    }

    // ------------ Задаем обстановку и следим за окончанием игры  ------------
    private void generateField() {
        _field.clear();

        // Добавить новый сезон        
        _field.setSeason(_season);
        _season.setCurrentSeason(Season.Seasons.SUMMER);
        _season.setCurrentDay(0);

        // добавить роботов
        if (littleRobot().position() == null) {
            _field.setLittleRobot(new CellPosition(4, 2), littleRobot());
        } else {
            _field.setLittleRobot(littleRobot().position(), littleRobot());
        }
        if (bigRobot().position() == null) {
            _field.setBigRobot(new CellPosition(4, 15), bigRobot());
        } else {
            _field.setBigRobot(bigRobot().position(), bigRobot());
        }

        // добавить целевую позицию
        // добавить стены        
        _field.addWall(new MiddlePosition(new CellPosition(1, 1), Direction.east()), new WallPiece(_field));
        for (int i = 3; i <= 7; i++) {
            _field.addWall(new MiddlePosition(new CellPosition(i, 1), Direction.east()), new WallPiece(_field));
        }
        _field.addWall(new MiddlePosition(new CellPosition(9, 1), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 2), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 2), Direction.east()), new WallPiece(_field));
        for (int i = 7; i <= 10; i++) {
            _field.addWall(new MiddlePosition(new CellPosition(i, 2), Direction.east()), new WallPiece(_field));
        }
        _field.addWall(new MiddlePosition(new CellPosition(3, 3), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 3), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 3), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 3), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(10, 3), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(3, 4), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 4), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 5), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 5), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 5), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(10, 5), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 6), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 6), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 6), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 6), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 7), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 7), Direction.east()), new WallPiece(_field));
        for (int i = 1; i <= 5; i++) {
            _field.addWall(new MiddlePosition(new CellPosition(i, 8), Direction.east()), new WallPiece(_field));
        }
        _field.addWall(new MiddlePosition(new CellPosition(7, 8), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 8), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 8), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 9), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 9), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 9), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 10), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 10), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 10), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 10), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 10), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(10, 12), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 13), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 13), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(7, 13), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 13), Direction.east()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 13), Direction.east()), new WallPiece(_field));

        _field.addWall(new MiddlePosition(new CellPosition(3, 3), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(3, 11), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(3, 12), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(3, 13), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 5), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(4, 7), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 3), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 6), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 8), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 9), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 11), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 12), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(5, 13), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 2), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 9), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 11), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 12), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(6, 13), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 2), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 6), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(8, 7), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 7), Direction.south()), new WallPiece(_field));
        _field.addWall(new MiddlePosition(new CellPosition(9, 9), Direction.south()), new WallPiece(_field));

        //добавить ландшафты
        Landscape l = new Swamp(_field);
        ArrayList<CellPosition> area = new ArrayList<CellPosition>();
        area.add(new CellPosition(2, 5));
        area.add(new CellPosition(2, 6));
        area.add(new CellPosition(2, 7));
        l.setArea(area);
        _field.addLandscape(l);

        l = new Swamp(_field);
        area = new ArrayList<CellPosition>();
        area.add(new CellPosition(7, 7));
        l.setArea(area);
        _field.addLandscape(l);

        l = new Swamp(_field);
        area = new ArrayList<CellPosition>();
        area.add(new CellPosition(4, 12));
        area.add(new CellPosition(5, 12));
        l.setArea(area);
        _field.addLandscape(l);

        l = new Swamp(_field);
        area = new ArrayList<CellPosition>();
        area.add(new CellPosition(7, 11));
        area.add(new CellPosition(8, 11));
        area.add(new CellPosition(7, 12));
        area.add(new CellPosition(8, 12));
        area.add(new CellPosition(7, 13));
        area.add(new CellPosition(8, 13));
        area.add(new CellPosition(9, 12));
        l.setArea(area);
        _field.addLandscape(l);

        l = new Sand(_field);
        area = new ArrayList<CellPosition>();
        area.add(new CellPosition(8, 4));
        area.add(new CellPosition(8, 5));
        area.add(new CellPosition(9, 4));
        area.add(new CellPosition(9, 5));
        l.setArea(area);
        _field.addLandscape(l);

        l = new Sand(_field);
        area = new ArrayList<CellPosition>();
        for (int i = 2; i <= 4; i++) {
            for (int j = 9; j <= 11; j++) {
                area.add(new CellPosition(i, j));
            }
        }
        l.setArea(area);
        _field.addLandscape(l);

        _field.fillEmptyCells();

        _targetPos = new CellPosition(9, 15);
    }

    private void identifyGameOver() {
        _gameOver = littleRobot().position().equals(bigRobot().position()) || littleRobot().position().equals(targetPosition()) || !(littleRobot().isAlive());
        _playerWon = littleRobot().position().equals(targetPosition()) && littleRobot().isAlive();
        if (_gameOver) {
            _gameOverListener.gameOver(new GameOverEvent(this));
        }
    }
    private boolean _gameOver;              /// закончилась ли игра
    private boolean _playerWon;             /// выиграл ли игрок

    /**
     * Выиграл ли игрок
     *
     * @return выиграл ли игрок
     */
    public boolean isPlayerWon() {
        return _playerWon;
    }

    private GameOverListener _gameOverListener;

    /**
     * Установить слушателя об окончании игры
     *
     * @param listener
     */
    public void setGameOverListener(GameOverListener listener) {
        _gameOverListener = listener;
    }

}
